#!/usr/bin/env python3
import re, subprocess, statistics, math, sys
from mpl_toolkits.basemap import Basemap
import matplotlib.pyplot as plt
from geopy.geocoders import Nominatim

vpns = ["Bombay", "California", "Frankfurt", "SaoPaulo", "Sydney", "Tokyo", "Virginia"]
lon = [72.8776559, -119.4179323,  8.6821267, -46.6291845, 151.2092955, 139.6917063, -78.6568942]
lat = [19.0759837, 36.7782610, 50.1109220, -23.5431786, -33.8688197, 35.6894875, 37.4315734]
ipVpn = ["35.154.33.45", "54.153.119.115", "35.156.81.40", "52.67.209.84", "52.64.127.163", "52.193.67.69", "52.91.247.121"]
times = []
countVpn = 0
for i in vpns:
    Myoutput = subprocess.check_output('ping -c 10 %s' % ipVpn[countVpn], shell=True)
    Myout = Myoutput.decode() 
    Mydata = re.findall(r'time=([0-9.]+)', Myout)
    y1 = []
    x1 = []
    for j in range(len(Mydata)):
        x1 = Mydata[j].split('=')
        y1.append(float(x1[0]))
    z1 = min(y1)/1000


    subprocess.call('sudo openvpn --config %s.ovpn --redirect-gateway def1 --daemon' % i, shell=True)
    subprocess.call('sleep 15', shell=True)
    output = subprocess.check_output('ping -c 10 %s' % sys.argv[1], shell=True)
    out = output.decode()

    data = re.findall(r'time=([0-9.]+)', out)
    y = []
    for j in range(len(data)):
        x = data[j].split('=')
        y.append(float(x[0]))
    z = min(y)/1000

    z = z - z1
    times.append(z)
    z = z*299792458
    z = z/2000
    z = z/5
    countVpn += 1
    subprocess.call('sudo killall openvpn', shell=True)
mins = []
for i in range(3):
    y = min(times)
    for j in range(len(times)):
        if y == times[j]:
            times[j] = 10
            mins.append( (y, lon[j], lat[j]) )
            break

total = mins[0][0] + mins[1][0] + mins[2][0]
wsp = []
for i in range(3):
    wspX = math.cos(mins[i][2]*(math.pi/180))*math.cos(mins[i][1]*(math.pi/180))
    wspY = math.cos(mins[i][2]*(math.pi/180))*math.sin(mins[i][1]*(math.pi/180))
    wspZ = math.sin(mins[i][2]*(math.pi/180))
    wsp.append( (wspX, wspY, wspZ) )
wsX = (wsp[0][0]*mins[0][0] + wsp[1][0]*mins[1][0] + wsp[2][0]*mins[2][0])/total
wsY = (wsp[0][1]*mins[0][0] + wsp[1][1]*mins[1][0] + wsp[2][1]*mins[2][0])/total
wsZ = (wsp[0][2]*mins[0][0] + wsp[1][2]*mins[1][0] + wsp[2][2]*mins[2][0])/total
Lon = math.atan2(wsY, wsX)
Hyp = math.sqrt(wsX*wsX + wsY*wsY)
Lat = math.atan2(wsZ, Hyp)
Lon = Lon*(180/math.pi)
Lat = Lat*(180/math.pi)
print(Lat, Lon)

outWhois = subprocess.check_output('whois %s' % sys.argv[1], shell=True)
decodeWhois = outWhois.decode()
data2W = re.findall(r'(address.*)\n', decodeWhois)
dataW = []
if data2W:
    for i in range(len(data2W)):
        if data2W[i] != "address range":
            tmp = re.split(": *", data2W[i])
            dataW.append(tmp[len(tmp)-1])

map = Basemap(projection='robin', lat_0=0, lon_0=0)
map.drawmapboundary(fill_color='aqua')
map.drawcoastlines()

for i in range(7):
    x, y = map(lon[i], lat[i])
    map.plot(x, y, 'ko', markersize=15)

x, y = map(Lon, Lat)
map.plot(x, y, 'yo', markersize=25)
geolocator = Nominatim()
location = geolocator.geocode("%s" % (dataW[0]))
if location !=None:
    print((location.latitude, location.longitude))
    x, y = map(location.longitude, location.latitude)
    map.plot(x, y, 'mo', markersize=25)
map.drawcountries()
map.drawlsmask(land_color = "#EC4354",
               ocean_color="#72A3E2",
               resolution = 'l')
plt.show()
